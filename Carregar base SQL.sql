-- Rodar pré carregamento da base depois de subir as apis na IDE
-- Cadastrando planos
INSERT INTO `clinica`.`plano`
(`id`,`num_cartao`,`empresa`,`tipo`)
VALUES
(1, '123456', 'Bradesco', 'Apartamento'),
(2, '987632', 'Porto Seguro', 'Apartamento'),
(3, '148576', 'Amil', 'Enfermaria');

-- Cadastrando medicos
INSERT INTO `clinica`.`medico`
(`id`,`nome`,`crm`,`especialidade`)
VALUES
(1,'Dr Livia',123456,'Psiquiatra'),
(2,'Dr Frutos',987643,'Ortopedista'),
(3,'Dr Marina',123456,'Clinico Geral');

-- Cadastrando Status
INSERT INTO `clinica`.`status`
(`id`,`nome`)
VALUES
(1,'Não Compareceu'),
(2,'Agendada'),
(3,'Realizada');