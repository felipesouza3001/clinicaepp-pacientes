package com.labsoftware.clinica.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "plano")
@Entity
@Getter
@Setter
public class Plano {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "num_cartao", length = 10)
    private String numCartao;

    @Column(name = "empresa", length = 45)
    private String empresa;

    @Column(name = "tipo", length = 45)
    private String tipo;
}