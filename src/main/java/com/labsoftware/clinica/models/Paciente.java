package com.labsoftware.clinica.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Table(name = "paciente", indexes = {
        @Index(name = "id_plano_idx", columnList = "id_plano")
})
@Entity
@Getter
@Setter
public class Paciente {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "cpf", length = 11)
    private String cpf;

    @Column(name = "nome", length = 45)
    private String nome;

    @Column(name = "data_nascimento")
    private LocalDate dataNascimento;

    @Column(name = "endereco", length = 45)
    private String endereco;

    @Column(name = "telefone1", length = 11)
    private String telefone1;

    @Column(name = "telefone2", length = 11)
    private String telefone2;

    @ManyToOne
    @JoinColumn(name = "id_plano")
    private Plano plano;
}