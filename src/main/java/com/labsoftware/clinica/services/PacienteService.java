package com.labsoftware.clinica.services;

import com.labsoftware.clinica.models.Paciente;
import com.labsoftware.clinica.repositories.PacienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
public class PacienteService {

    @Autowired
    private PacienteRepository pacienteRepository;


    public Paciente cadastrar(Paciente paciente) {
        return pacienteRepository.save(paciente);
    }

    //TODO:  Melhorar busca por palavras soltas como: Fran, Glinglani,
    public Paciente consultaPorNome(String nomePaciente) {

        Optional<Paciente> retorno = pacienteRepository.findByNome(nomePaciente);

        if (!retorno.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Paciente não existe na base de dados");
        }

        return retorno.get();
    }

    public Paciente consultaPorCpf(String cpf) {
        Optional<Paciente> retorno = pacienteRepository.findByCpf(cpf);

        if (!retorno.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Paciente não existe na base de dados");
        }

        return retorno.get();
    }

    public Paciente alterar(int idPaciente, String status) {
        //return pacienteRepository.save(paciente);
        return null;
    }


}
