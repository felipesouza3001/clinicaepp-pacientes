package com.labsoftware.clinica.repositories;

import com.labsoftware.clinica.models.Paciente;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface PacienteRepository extends CrudRepository<Paciente, Integer> {

    Optional<Paciente> findByCpf(String cpf);

    @Query(value= "SELECT * FROM paciente WHERE nome like %?1%", nativeQuery = true)
    Optional<Paciente> findByNome(String nome);
}
