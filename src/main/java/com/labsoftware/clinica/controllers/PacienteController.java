package com.labsoftware.clinica.controllers;

import com.labsoftware.clinica.models.Paciente;
import com.labsoftware.clinica.services.PacienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/clinica/paciente")
public class PacienteController {


    @Autowired
    PacienteService pacienteService;

    @PostMapping("/cadastrar")
    public Paciente cadastrar(@RequestBody Paciente paciente) {
        return pacienteService.cadastrar(paciente);
    }

    @GetMapping("/consultar")
    public Paciente consultar(@RequestParam String busca, @RequestParam String tipo) {
        if (tipo.equals("nome")) {
            return pacienteService.consultaPorNome(busca);
        } else
            return pacienteService.consultaPorCpf(busca);
    }

    //TODO: Falta finalizar endpoint de atualizar dados paciente
    @PatchMapping("/alterar/{idPaciente}")
    public Paciente alterar(@PathVariable int idPaciente, @RequestBody Map<String, String> valor) {

        return pacienteService.alterar(idPaciente, valor.get("status"));

    }

}
